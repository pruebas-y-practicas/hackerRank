package hackerRank.dataStructure.arrays

import org.scalatest.FunSuite

/**
  * Created by londo on 26/09/16.
  */
class Array2DTest extends FunSuite {

  test("read: originial") {
    val input = """1 1 1 0 0 0
                  |0 1 0 0 0 0
                  |1 1 1 0 0 0
                  |0 0 2 4 4 0
                  |0 0 0 2 0 0
                  |0 0 1 2 4 0""".stripMargin
    val output = List(List(1, 1, 1, 0, 0, 0), List(0, 1, 0, 0, 0, 0), List(1, 1, 1, 0, 0, 0),
      List(0, 0, 2, 4, 4, 0), List(0, 0, 0, 2, 0, 0), List(0, 0, 1, 2, 4, 0))
    assert(Array2D.read(input) == output)
  }

  test("process: originial") {
    val input = List(List(1, 1, 1, 0, 0, 0), List(0, 1, 0, 0, 0, 0), List(1, 1, 1, 0, 0, 0),
      List(0, 0, 2, 4, 4, 0), List(0, 0, 0, 2, 0, 0), List(0, 0, 1, 2, 4, 0))
    val output = 19
    assert(Array2D.process(input) == output)
  }

  test("hourglass: originial") {
    val input = List(List(1, 1, 1, 0, 0, 0), List(0, 1, 0, 0, 0, 0), List(1, 1, 1, 0, 0, 0),
      List(0, 0, 2, 4, 4, 0), List(0, 0, 0, 2, 0, 0), List(0, 0, 1, 2, 4, 0))
    assert(Array2D.hourglass(4, 3, input) == 19)
    assert(Array2D.hourglass(3, 4, input) == 6)
    assert(Array2D.hourglass(1, 1, input) == 7)
  }

}
