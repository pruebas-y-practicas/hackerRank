package hackerRank.algorithms.strings

import org.scalatest.FunSuite

/**
  * Created by londo on 27/09/16.
  */
class GemstonesTest extends FunSuite {

  test("read: original") {
    val input = """3
                  |abcdde
                  |baccd
                  |eeabg""".stripMargin
    val output = List("abcdde", "baccd", "eeabg")
    assert(Gemstones.read(input) == output)
  }

  test("commonElemments: original") {
    val input = List("abcdde", "baccd", "eeabg")
    val output = "ab"
    assert(Gemstones.commonElements(input) == output)
  }

}
