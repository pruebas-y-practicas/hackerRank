package hackerRank.algorithms.strings

import org.scalatest.FunSuite

/**
  * Created by londo on 26/09/16.
  */
class PangramsTest extends FunSuite {

  test("isPangram: original 1") {
    val input = "We promptly judged antique ivory buckles for the next prize"
    assert(Pangrams.isPangram(input))
  }

  test("isPangram: original 2") {
    val input = "We promptly judged antique ivory buckles for the prize"
    assert(!Pangrams.isPangram(input))
  }

}
