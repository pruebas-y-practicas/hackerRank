package hackerRank.algorithms.strings

import org.scalatest.FunSuite

/**
  * Created by londo on 26/09/16.
  */
class SuperReducedTest extends FunSuite {

  test("reduce: original 1") {
    val input = "aaabccddd"
    val output = "abd"
    assert(SuperReduced.reduce(input) == output)
  }

  test("reduce: original 2") {
    val input = "baab"
    val output = "Empty String"
    assert(SuperReduced.reduce(input) == output)
  }

  test("reduce: original 3") {
    val input = "aa"
    val output = "Empty String"
    assert(SuperReduced.reduce(input) == output)
  }

  test("reduce: manual 1") {
    val input = ""
    val output = "Empty String"
    assert(SuperReduced.reduce(input) == output)
  }

}
