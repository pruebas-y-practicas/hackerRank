package hackerRank.algorithms.strings

import org.scalatest.FunSuite

/**
  * Created by londo on 27/09/16.
  */
class AlternandoCaracteresTest extends FunSuite {

  test("read: original") {
    val input = """5
                  |AAAA
                  |BBBBB
                  |ABABABAB
                  |BABABA
                  |AAABBB""".stripMargin
    val output = List("AAAA", "BBBBB", "ABABABAB", "BABABA", "AAABBB")
    assert(AlternandoCaracteres.read(input) == output)
  }

  test("alternador: original") {
    val input = List("AAAA", "BBBBB", "ABABABAB", "BABABA", "AAABBB")
    val output = List(3, 4, 0, 0, 4)
    assert(AlternandoCaracteres.alternador(input) == output)
  }

}
