package hackerRank.tutorial.days30

/**
  * Created by londo on 26/09/16.
  * https://www.hackerrank.com/challenges/30-loops/submissions/code/28833121
  */
object Day5 {

  def main(args: Array[String]) {
    val sc = new java.util.Scanner(System.in)
    val n = sc.nextInt()
    for (i <- 1 to 10) {
      println(s"$n x $i = ${n * i}")
    }
  }
}
