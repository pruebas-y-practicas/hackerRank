package hackerRank.tutorial.days30

/**
  * Created by londo on 26/09/16.
  */
object Day6 {

  def main(args: Array[String]): Unit = {
    val input = scala.io.Source.fromInputStream(System.in).mkString
    val strings = read(input)
    val res = process(strings)
    res.foreach(s => println(s"${s._1} ${s._2}"))
  }

  def read(input: String): List[String] = {
    val sc = new java.util.Scanner(input)
    val n = sc.nextInt()
    val list = for (i <- 1 to n) yield sc.next()
    list.toList
  }

  def process(words: List[String]): List[(String, String)] = {
    @scala.annotation.tailrec
    def rec(a: String, b: String, word: List[Char]): (String, String) = {
      word match {
        case Nil => a -> b
        case x :: Nil => rec(a + x, b, Nil)
        case x :: y :: tail => rec(a + x, b + y, tail)
      }
    }
    words.map(w => rec("", "", w.toList))
  }

}
