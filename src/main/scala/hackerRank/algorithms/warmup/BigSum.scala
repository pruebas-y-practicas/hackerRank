package hackerRank.algorithms.warmup

/**
  * Created by londo on 17/09/16.
  * https://www.hackerrank.com/challenges/a-very-big-sum
  */
object BigSum {

  def main(args: Array[String]) {
    val input = scala.io.Source.fromInputStream(System.in).mkString
    val sum = process(read(input))
    println(sum)
  }

  def read(input: String): List[Int] = {
    val sc = new java.util.Scanner(input)
    val n = sc.nextInt()
    val arr = new Array[Int](n)
    for(arr_i <- 0 until n) {
      arr(arr_i) = sc.nextInt()
    }
    arr.toList
  }

  def process(list: List[Int]): Long = {
    list.foldRight(0L)((item, acum) => acum + item)
  }
}
