package hackerRank.algorithms.strings

/**
  * Created by londo on 27/09/16.
  */
object AlternandoCaracteres {

  def main(args: Array[String]): Unit = {
    val input = scala.io.Source.fromInputStream(System.in).mkString
    val words = read(input)
    val res = alternador(words)
    res.foreach(println)
  }

  def read(input: String): List[String] = {
    val sc = new java.util.Scanner(input)
    val n = sc.nextInt()
    val list = for (i <- 1 to n) yield sc.next()
    list.toList
  }

  def alternador(list: List[String]): List[Int] = {
    @scala.annotation.tailrec
    def rec(word: List[Char], removed: Int): Int = {
      word match {
        case Nil => removed
        case a :: Nil => removed
        case a :: b :: tail if a == b => rec(b :: tail, removed + 1)
        case a :: b :: tail => rec(b :: tail, removed)
      }
    }
    list.map(s => rec(s.toList, 0))
  }

}
