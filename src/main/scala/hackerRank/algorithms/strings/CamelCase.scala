package hackerRank.algorithms.strings


/**
  * Created by londo on 17/09/16.
  * https://www.hackerrank.com/challenges/camelcase
  */
object CamelCase {

  def main(args: Array[String]) {
    val sc = new java.util.Scanner (System.in)
    val s = sc.next()
    println(wordsCount(s))
  }

  def wordsCount(string: String): Int = {
    import scala.util.matching.Regex
    new Regex("[A-Z]").findAllIn(string).size + 1
  }

}
