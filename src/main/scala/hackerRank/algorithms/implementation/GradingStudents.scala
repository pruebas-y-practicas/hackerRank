package hackerRank.algorithms.implementation

import scala.io.Source

/**
  * Created by londo on 17/09/16.
  * https://www.hackerrank.com/challenges/grading/problem
  */
object Result {

  /*
   * Complete the 'gradingStudents' function below.
   *
   * The function is expected to return an INTEGER_ARRAY.
   * The function accepts INTEGER_ARRAY grades as parameter.
   */

  def gradingStudents(grades: Array[Int]): Array[Int] = {
    grades.map(grade => {
      if (grade < 38) grade
      else {
        val next5 = ((grade / 5) + 1) * 5
        if (next5 - grade < 3) next5
        else grade
      }
    })
  }

}

object GradingStudents {
  def main(args: Array[String]): Unit = {
//    val printWriter = new PrintWriter(sys.env("OUTPUT_PATH"))
//
//    val gradesCount = StdIn.readLine.trim.toInt
//
//    val grades = Array.ofDim[Int](gradesCount)
//
//    for (i <- 0 until gradesCount) {
//      val gradesItem = StdIn.readLine.trim.toInt
//      grades(i) = gradesItem
//    }

    val grades = Array(73,67,38,33)
    val result = Result.gradingStudents(grades)
    println(result.mkString("\n"))

//    printWriter.println(result.mkString("\n"))
//
//    printWriter.close()
  }
}

